package nl.praegus.calculator;

import org.springframework.stereotype.Component;

@Component
public class CalculatorService {

    public double getPi() {
        return 3.141592;
    }

    public double telOp(double i, double j) {
        return i + j;
    }

    public double trekAf(double i, double j) {
        return i - j;
    }

    public double vermenigvuldig(Double i, Double j) {
        return i * j;
    }

    public double deel(double i, double j) {
        return i / j;
    }
}
