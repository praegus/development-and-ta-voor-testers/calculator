package nl.praegus.calculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.boot.autoconfigure.web.EmbeddedServletContainerAutoConfiguration;
import org.springframework.boot.autoconfigure.web.ServerPropertiesAutoConfiguration;
import org.springframework.context.annotation.Import;

//@EnableAutoConfiguration
@Import({
    EmbeddedServletContainerAutoConfiguration.class,
    ServerPropertiesAutoConfiguration.class,
    DispatcherServletAutoConfiguration.class,
    CalculatorConfiguration.class
})

public class CalculatorApplication {


    public static void main(String[] args) {
        SpringApplication.run(CalculatorController.class, args);
    }
}
