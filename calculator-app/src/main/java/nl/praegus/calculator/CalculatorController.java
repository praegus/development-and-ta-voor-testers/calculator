package nl.praegus.calculator;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.naming.ServiceUnavailableException;

@Controller
@EnableAutoConfiguration
@RequestMapping("/api/calculator")
public class CalculatorController {

    private CalculatorService calculatorService;

    public CalculatorController() {
        this.calculatorService = new CalculatorService();
    }

    @GetMapping("/pi")
    @ResponseBody
    public double getPi() {
        return calculatorService.getPi();
    }

    @ResponseBody
    @GetMapping(value = "/telop/{i}/{j}")
    public double getOptelling(@PathVariable("i") double i, @PathVariable("j") double j) {
        return calculatorService.telOp(i, j);
    }

    @ResponseBody
    @GetMapping(value = "/trekaf/{i}/{j}")
    public double getAftrekking(@PathVariable("i") double i, @PathVariable("j") double j) {
        return calculatorService.trekAf(i, j);
    }

    @ResponseBody
    @GetMapping(value = "/vermenigvuldig/{i}/{j}")
    public double getVermenigvuldiging(@PathVariable("i") double i, @PathVariable("j") double j) {
        return calculatorService.vermenigvuldig(i, j);
    }

    @ResponseBody
    @GetMapping(value = "/deel/{i}/{j}")
    public double getDeling(@PathVariable("i") double i, @PathVariable("j") double j) {
        if(i==6){
            calculatorService.vermenigvuldig(1.0, 2.0);
        }
        return calculatorService.deel(i, j);
    }
}