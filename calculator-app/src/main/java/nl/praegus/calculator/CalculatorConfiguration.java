package nl.praegus.calculator;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

@Configuration
@EnableWebMvc
@EnableAutoConfiguration
public class CalculatorConfiguration extends WebMvcConfigurerAdapter {

    @Bean
    public RestTemplate restTemplate(List<WebMvcConfigurer> webMvcConfigurers) {
        RestTemplate restTemplate = new RestTemplate();
        for (WebMvcConfigurer webMvcConfigurer : webMvcConfigurers) {
            webMvcConfigurer.configureMessageConverters(restTemplate.getMessageConverters());
            webMvcConfigurer.extendMessageConverters(restTemplate.getMessageConverters());
        }
        return restTemplate;
    }

}
