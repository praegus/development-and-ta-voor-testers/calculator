package nl.praegus.calculator;

import org.junit.Test;

import static jdk.nashorn.internal.objects.Global.Infinity;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class CalculatorServiceTest {

    private final CalculatorService calculatorService = new CalculatorService();

    @Test
    public void telOp() {
        assertThat(calculatorService.telOp(1, 2)).isEqualTo(3);
        assertThat(calculatorService.telOp(1, 6)).isEqualTo(7);
        assertThat(calculatorService.telOp(100, 200)).isEqualTo(300);
    }

    @Test
    public void trekAf() {
        assertThat(calculatorService.trekAf(3, 4)).isEqualTo(-1);
    }

    @Test
    public void vermenigvuldig() {
        assertThat(calculatorService.vermenigvuldig(3.0, 6.0)).isEqualTo(18);
    }

    @Test
    public void deel() {
        assertThat(calculatorService.deel(3, 3)).isEqualTo(1);
        assertThat(calculatorService.deel(3, 0)).isEqualTo(Infinity);
    }

    @Test
    public void als_ik_pi_ophaal_dan_krijg_ik_3_punt_141592_terug(){
        calculatorService.getPi();
    }
}
