package nl.praegus.calculator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CalculatorControllerTest {

    @InjectMocks
    CalculatorController controller = new CalculatorController();

    @Mock
    private CalculatorService calculatorService;


    @Test
    public void als_ik_de_deelfunctionaliteit_aanroep_dan_wordt_de_service_aangeroepen(){
        when(calculatorService.deel(1,2)).thenReturn(3.0);
//        when(calculatorService.vermenigvuldig(any(),any())).thenReturn(3.0);

        double response = controller.getDeling(1, 2);

        assertThat(response).isEqualTo(3.0);
        verify(calculatorService, times(1)).deel(1,2);
        verify(calculatorService, times(0)).vermenigvuldig(any(), any());
    }


}