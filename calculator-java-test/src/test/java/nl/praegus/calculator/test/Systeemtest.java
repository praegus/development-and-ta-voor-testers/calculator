package nl.praegus.calculator.test;

import nl.praegus.calculator.CalculatorApplication;
import nl.praegus.calculator.CalculatorConfiguration;
import nl.praegus.calculator.CalculatorController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {CalculatorApplication.class, CalculatorConfiguration.class, CalculatorController.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class Systeemtest {

    @LocalServerPort
    private int PORT;
    private String LOCALHOST;

    @Autowired
    private RestTemplate restTemplate;

    @Before
    public void setUrl() {
        LOCALHOST = "http://localhost:" + PORT + "/api/calculator";
    }

    @Test
    public void als_ik_een_en_twee_optel_dan_krijg_ik_drie() {
        int optelling = restTemplate.getForObject(LOCALHOST + "/telop/" + 7 + "/" + -10, Integer.class);

        assertThat(optelling).isEqualTo(-3);
    }

    @Test
    public void als_ik_drie_en_vier_van_elkaar_aftrek_dan_krijg_ik_min_een() {
         int aftrekking = restTemplate.getForObject(LOCALHOST + "/trekaf/" + 3 + "/" + 4, Integer.class);
        assertThat(aftrekking).isEqualTo(-1);
    }

    @Test
    public void als_ik_drie_en_vier_vermenigvuldig_dan_krijg_ik_twaalf() {
        int vermenigvuldiging = restTemplate.getForObject(LOCALHOST + "/vermenigvuldig/" + 3 + "/" + 4, Integer.class);
        assertThat(vermenigvuldiging).isEqualTo(12);
    }

    @Test
    public void alle_optellingen_tussen_min_10_en_10_doen_het(){
        for(int i=-10;i<10;i++){
            for(int j=-10;j<10;j++){
                int optelling = restTemplate.getForObject(LOCALHOST + "/telop/" + i + "/" + j, Integer.class);

                System.out.println("i: "+ i + " j: "+j);
                assertThat(optelling).isEqualTo(i+j);
            }
        }
    }
}
