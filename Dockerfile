docker pull ibmjava:sfj-alpine
FROM ibmjava:jre
RUN mkdir /opt/app
COPY calculator-app/target/calculator-app-1.0-SNAPSHOT.jar /opt/app/app.jar
CMD ["java", "-jar", "/opt/app/app.jar"]
EXPOSE 8080l